{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE UnicodeSyntax #-}
{-# LANGUAGE ScopedTypeVariables #-}

module AnalysisSpec
  ( htf_thisModulesTests
  ) where

import Analysis
import Data
import Utils

import Control.Lens
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Arbitrary ()
import Data.Time (UTCTime)
import Test.Framework
import Test.QuickCheck.Instances.Time

nonEmptyString :: Gen String
nonEmptyString = listOf1 arbitrary

nonEmptyText :: Gen Text
nonEmptyText = nonEmptyString <&> T.pack

arbitraryDate = (arbitrary :: Gen UTCTime) <&> formatDateTime

instance Arbitrary Post where
  arbitrary = do
    _author <- nonEmptyText
    Positive _likes <- arbitrary
    Positive _reposts <- arbitrary
    Positive _replies <- arbitrary
    let _ratioLikesVsReplies = fromIntegral _likes / fromIntegral _replies
    _text <- arbitrary
    _isRepost <- arbitrary
    _dateRaw <- arbitrary
    _date <- arbitraryDate
    return $ Post {..}

instance Arbitrary SocialNetworkType where
  arbitrary = do
    (rngInt :: Int) <- arbitrary
    let idx = rngInt `mod` 2
    return $ idxToSNT idx
    where
      idxToSNT 0 = Gab
      idxToSNT 1 = Twitter

instance Arbitrary FatPost where
  arbitrary = do
    Post {..} <- arbitrary
    _socialNetwork <- arbitrary
    _obtainedAt <- arbitraryDate
    return $ FatPost {..}

test_removeGabPostLink = do
  assertEqual "" (removeGabPostLink "https://gab.com/BrandonChimaria/posts/102870096722300276")
  assertEqual "x  y" (removeGabPostLink "x https://gab.com/BrandonChimaria/posts/102870096722300276 y")
  assertEqual "x y" (removeGabPostLink "xhttps://gab.com/BrandonChimaria/posts/102870096722300276 y")
  assertEqual "#oc\n " (removeGabPostLink "#oc\nhttps://gab.com/menfon/posts/102531322026555181 ")

prop_isEmptyPost :: Post -> Bool
prop_isEmptyPost p = isEmptyPost p == (p ^. text == "")

prop_filterPostsForAnalysis :: [Post] -> Bool
prop_filterPostsForAnalysis xs =
  all (\post -> not (post ^. isRepost) && not (isEmptyPost post)) (filterPostsForAnalysis xs)

-- prop_calculateMatchingScoreFromEdits :: Text -> Text -> Int -> Bool
prop_calculateMatchingScoreFromEdits = withQCArgs (\a -> a {maxSuccess = 1000}) test
  where
    test x y (NonNegative edits) =
      edits < maxLen && not (T.null x) && not (T.null y) ==> whenFail (putStrLn $ "res = " <> show res) $
      resInRange && resLooksFine && swappedInputsGiveSameResult
      where
        res = calculateMatchingScoreFromEdits x y edits
        maxLen = max (T.length x) (T.length y)
        resInRange = res >= 0.0 && res <= 1.0
        resLooksFine =
          if | edits == 0 -> res == 1.0
             | edits == maxLen -> res == 0.0
             | edits >= 1 -> res < 1.0
        invRes = calculateMatchingScoreFromEdits y x edits
        swappedInputsGiveSameResult = res == invRes

test_calculateMatchingScoreFromEdits = do
  assertEqual 1.0 (calculateMatchingScoreFromEdits "a" "a" 0)
  assertEqual 0.0 (calculateMatchingScoreFromEdits "a" "b" 1)
  assertEqual 0.5 (calculateMatchingScoreFromEdits "ab" "xb" 1)
  let dog1 = "  #photo #dog #photography #amateur #oc"
  let dog2 = "  #photo #dog #photography #amateur #oc\n xXxXx"
  assertBool $ calculateMatchingScoreFromEdits dog1 dog2 7 > 0.8

prop_calculateMatchingScore = withQCArgs (\a -> a {maxSuccess = 2000}) test
  where
    test x y =
      whenFail (putStrLn $ "score = " <> show score) $
      classify (score == 1) "score = 1" $
      classify (score == 0) "score = 0" $
      classify textsSame "texts same" $
      if | score == 1 -> textsSame
         | score < 1 -> not textsSame
      where
        score = calculateMatchingScore x y
        textsSame = x ^. text == y ^. text
        -- noCommonChars = all (\a -> a `notElem` (y ^. text & T.unpack)) (x ^. text & T.unpack)
