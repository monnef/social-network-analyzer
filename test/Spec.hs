{-# OPTIONS_GHC -F -pgmF htfpp #-}

import {-@ HTF_TESTS @-} AnalysisSpec
import {-@ HTF_TESTS @-} UtilsSpec

import Test.Framework

main :: IO ()
main = htfMain htf_importedTests
