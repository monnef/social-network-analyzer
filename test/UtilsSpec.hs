{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE ScopedTypeVariables #-}

module UtilsSpec
  ( htf_thisModulesTests
  ) where

import           Utils

import           Control.Lens
import           Data.Maybe                     (isJust, isNothing)
import           Data.Text                      (Text)
import qualified Data.Text                      as T
import           Data.Text.Arbitrary            ()
import           Data.Time                      (UTCTime (..), getCurrentTime,
                                                 utctDay, utctDayTime, secondsToDiffTime)
import           Test.Framework
import           Test.QuickCheck.Instances.Time
import           Text.Read                      (readMaybe)

prop_numTextToIntOrZero :: WithQCArgs (Text -> Property)
prop_numTextToIntOrZero = withQCArgs (\a -> a {maxSuccess = 5000}) test
  where
    test x =
      classify (x == "") "empty" $
      classify (isNothing readVal) "invalid" $
      classify (isJust readVal) "valid" $
      if | x == "" -> res === 0
         | otherwise ->
           case readVal of
             Nothing -> res === 0
             Just x  -> res === x
      where
        readVal = readMaybe (T.unpack x)
        res = numTextToIntOrZero x

test_combPairs = do
  assertEqual ([] :: [(Int, Int)]) (combPairs ([] :: [[Int]]))
  assertEqual [("a", "b")] (combPairs [["a"], ["b"]])
  assertEqual [("a", "b"), ("a", "c"), ("b", "c")] (combPairs [["a"], ["b"], ["c"]])
  assertEqual
    [ ("a1", "b1")
    , ("a1", "b2")
    , ("a1", "c1")
    , ("a1", "c2")
    , ("a2", "b1")
    , ("a2", "b2")
    , ("a2", "c1")
    , ("a2", "c2")
    , ("b1", "c1")
    , ("b1", "c2")
    , ("b2", "c1")
    , ("b2", "c2")
    ]
    (combPairs [["a1", "a2"], ["b1", "b2"], ["c1", "c2"]])

test_formatDateTime_parseDateTime = do
  cur <- getCurrentTime
  putStrLn $ "cur = " <> show cur
  let formatted = formatDateTime cur
  putStrLn $ T.unpack $ "formatted = " <> formatted
  let parsed = parseDateTime formatted
  putStrLn $ "parsed = " <> show parsed
  let curDayTime :: Integer = cur & utctDayTime & toRational & floor
  let curWithoutSeconds = curDayTime & (`div` 60) & (* 60) & secondsToDiffTime & UTCTime (utctDay cur)
  assertEqual curWithoutSeconds parsed
