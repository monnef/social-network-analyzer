{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE UnicodeSyntax     #-}

module CmdArgsParser where

import           Data
import           Utils

import           Data.Text                     (Text)
import qualified Data.Text                     as T
import           Options.Applicative
import qualified Options.Applicative           as OA
import           Text.InterpolatedString.Perl6 (qq)
import           Text.PrettyPrint.ANSI.Leijen  as PP
import           Text.Read                     (readMaybe)

readInt :: String -> Maybe Int
readInt = readMaybe

readText :: String -> Maybe Text
readText = T.pack >>> Just

intMaybeReader :: ReadM (Maybe Int)
intMaybeReader = maybeReader (readInt >>> pure)

textMaybeReader :: ReadM (Maybe Text)
textMaybeReader = maybeReader (readText >>> pure)

cmdArgsParserInfo :: ParserInfo CmdArgs
cmdArgsParserInfo = OA.info (cmdArgsParser <**> helper) (fullDesc <> progDesc "TODO" <> footerDoc (Just footerText))
  where
    footerText = example <> linebreak <> linebreak <> linebreak <> madeBy <> linebreak
    example =
      "Example:" <> linebreak <> dquote <> white "social-network-analyzer gabs.json tweets.json" <> dquote <+> "TODO"
    madeBy = "  Made by" <+> magenta "monnef" <> ". Licensed under GPLv3." <+> copyleft
    copyleft = "\127279" <+> "2019"

cmdArgsParser :: Parser CmdArgs
cmdArgsParser =
  CmdArgs OA.<$> --
  argument str (metavar "FIRST-FILE" <> firstFileHelp) <*>
  argument str (metavar "SECOND-FILE" <> secondFileHelp) <*>
  option auto (short 't' <> long "threshold" <> value 0.7 <> metavar "THRESHOLD" <> thresholdHelp) <*>
  flag True False (long "disable-remove-gab-links" <> disableRemoveGabLinks) <*>
  option auto (short 'd' <> long "max-hours-diff" <> value 24 <> metavar "HOURS" <> maxDiffHelp) <*>
  switch (long "verbose" <> short 'v' <> verboseHelp)
  where
    firstFileHelp = help "TODO"
    secondFileHelp = help "TODO"
    thresholdHelp = help "TODO"
    disableRemoveGabLinks = help "TODO"
    maxDiffHelp = help "TODO"
    verboseHelp = help "Enable verbose output"

parseCmdArgs :: IO CmdArgs
parseCmdArgs = customExecParser (prefs showHelpOnEmpty) cmdArgsParserInfo
