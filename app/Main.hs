{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Main where

import           Analysis
import           CmdArgsParser
import           Data
import           Exceptions
import           FileLoader
import           Logging
import           Utils

import           Control.Lens                  hiding ((&))
import           Data.Aeson.Encode.Pretty      (encodePretty)
import qualified Data.ByteString.Lazy.Char8    as CL
import qualified Data.Text                     as T
import           Text.InterpolatedString.Perl6 (qq)

main :: IO ()
main = do
  cmdArgs <- parseCmdArgs
  let log = makeLogger cmdArgs
  log $ tshow cmdArgs
  f1 <- loadFile (cmdArgs ^. firstFileName) <&> orCrash
  f2 <- loadFile (cmdArgs ^. secondFileName) <&> orCrash
  let inputs = [f1, f2]
  log $ inputs <&> (\x -> tshow (x ^. socialNetwork) <> ":" <> (x ^. userName)) & T.intercalate ", "
  let res = analyze cmdArgs inputs
  res & encodePretty & CL.unpack & putStrLn
