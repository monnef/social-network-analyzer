{-# LANGUAGE OverloadedStrings #-}

module FileLoader
  ( loadFile
  ) where

import           Data
import           Exceptions
import           Utils

import           Control.Exception       (SomeException, handle, try)
import           Data.Aeson              (decode, eitherDecode)
import           Data.Bifunctor          (bimap)
import qualified Data.ByteString.Lazy    as BL
import qualified Data.Text               as T
import qualified Data.Text.Encoding      as TE
import qualified Data.Text.Lazy.Encoding as TLE

loadFile :: Text -> IO (Either AppException ExtractorResult)
loadFile fName = do
  res <- handle (\e -> mkFailedToLoadInputFile fName (e :: SomeException) & Left & return) (tReadFile fName <&> Right)
  return $ res & toRes
  where
    toRes :: Either AppException Text -> Either AppException ExtractorResult
    toRes r = do
      r' <- r
      eitherDecode (TE.encodeUtf8 r' & BL.fromStrict) & bimap (T.pack >>> FailedToParseInputFile fName) id
