{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module Exceptions where

import           Utils

import           Control.Exception             (Exception, SomeException)
import           Data.Text                     (Text)
import qualified Data.Text                     as T
import           Text.InterpolatedString.Perl6 (qq)

data AppException
  = FailedToLoadInputFile Text Text
  | FailedToParseInputFile Text Text

instance Show AppException where
  show (FailedToLoadInputFile x y) = [qq|Failed to load input file "$x": $y.|] & T.unpack
  show (FailedToParseInputFile x y) = [qq|Failed to load input file "$x": $y.|] & T.unpack

instance Exception AppException

mkFailedToLoadInputFile :: Exception e => Text -> e -> AppException
mkFailedToLoadInputFile fName e = FailedToLoadInputFile fName (tshow e)

mkFailedToParseInputFile :: Exception e => Text -> e -> AppException
mkFailedToParseInputFile fName e = FailedToParseInputFile fName (tshow e)

orCrash :: Either AppException a -> a
orCrash (Right x) = x
orCrash (Left x) = error $ show x
