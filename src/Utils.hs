{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module Utils
  ( tshow
  , tPutStrLn
  , tReadFile
  , (>>>)
  , (&)
  , (<&>)
  , Text
  , sleep
  , numTextToIntOrZero
  , dropPrefix
  , handleToName
  , nameToHandle
  , bool
  , formatDateTime
  , parseDateTime
  , combPairs
  ) where

import           Control.Arrow              ((>>>))
import qualified Control.Arrow              as CA
import           Control.Concurrent         (threadDelay)
import           Data.Bool                  (bool)
import qualified Data.ByteString.Lazy       as BL
import qualified Data.ByteString.Lazy.Char8 as CL
import           Data.Function              ((&))
import           Data.Functor               ((<&>))
import           Data.Maybe                 (fromMaybe)
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import qualified Data.Text.Lazy             as TL
import qualified Data.Text.Lazy.Encoding    as TLE
import           Data.Time                  (FormatTime, ParseTime, UTCTime,
                                             defaultTimeLocale, formatTime,
                                             iso8601DateFormat,
                                             parseTimeOrError)
import           Text.Read                  (readMaybe)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

tReadFile :: Text -> IO Text
tReadFile = T.unpack >>> readFile >>> fmap T.pack

mapTriple :: (a -> d, b -> e, c -> f) -> (a, b, c) -> (d, e, f)
mapTriple ~(f, g, h) ~(a, b, c) = (f a, g b, h c)

sleep :: Double -> IO ()
sleep secs = threadDelay $ round $ secs * 1000 * 1000

numTextToIntOrZero :: Text -> Int
numTextToIntOrZero = T.unpack >>> readMaybe >>> fromMaybe 0

dropPrefix :: Text -> Text -> Text
dropPrefix prefix t = fromMaybe t (T.stripPrefix prefix t)

handleToName :: Text -> Text
handleToName = dropPrefix "@"

nameToHandle :: Text -> Text
nameToHandle = handleToName >>> ("@" <>)

formatIso8601WithTime = iso8601DateFormat $ Just "%H:%M%EZ"

formatDateTime :: FormatTime t => t -> Text
formatDateTime = formatTime defaultTimeLocale formatIso8601WithTime >>> T.pack

parseDateTime :: ParseTime t => Text -> t
-- Broken time library: formatted text cannot be parsed using same format string
-- parseDateTime = T.unpack >>> parseTimeOrError False defaultTimeLocale formatIso8601WithTime
parseDateTime x =
  x & T.stripSuffix "Z" & fromMaybe x & T.unpack &
  parseTimeOrError False defaultTimeLocale (iso8601DateFormat $ Just "%H:%M")

combPairs :: [[a]] -> [(a, a)]
combPairs [] = []
combPairs (x:xs) = process x xs & (<> [combPairs xs]) & concat
  where
    process :: [a] -> [[a]] -> [[(a, a)]]
    process cur others = cur <&> flip processOne others & concat
    processOne :: a -> [[a]] -> [[(a, a)]]
    processOne x yss = yss <&> (<&> (x, ))
