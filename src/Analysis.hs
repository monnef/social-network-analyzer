{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TupleSections     #-}

module Analysis where

import           Data
import           Utils

import           Control.Lens
import           Data.Maybe        (catMaybes)
import qualified Data.Text         as T
import           Data.Time         (diffUTCTime)
import           Text.EditDistance (defaultEditCosts,
                                    restrictedDamerauLevenshteinDistance)
import           Text.RE.PCRE.Text (ed, (*=~/))

calculateMatchingScoreFromEdits :: Text -> Text -> Int -> Double
calculateMatchingScoreFromEdits x y edits = 1.0 - fromIntegral edits' / fromIntegral maxLen'
  where
    maxLen = max (T.length x) (T.length y)
    maxLen' = bool maxLen 1 (T.null y)
    edits' =
      if edits < 0
        then error "Negative number of edits."
        else edits

calculateMatchingScore :: FatPost -> FatPost -> Double
calculateMatchingScore x y = calculateMatchingScoreFromEdits xT yT diffs
  where
    diffs = restrictedDamerauLevenshteinDistance defaultEditCosts (xT & T.unpack) (yT & T.unpack)
    xT = x ^. text
    yT = y ^. text

isEmptyPost :: Post -> Bool
isEmptyPost = view text >>> T.null

isPostRepost :: Post -> Bool
isPostRepost = view isRepost

filterPostsForAnalysis :: [Post] -> [Post]
filterPostsForAnalysis = filter f
  where
    preds :: [Post -> Bool]
    preds = [isEmptyPost >>> not, isPostRepost >>> not]
    f :: Post -> Bool
    f x = and $ preds <&> ($ x)

removeGabPostLink :: Text -> Text
removeGabPostLink = (*=~/ [ed|https?://gab.com/\w+/posts/\w*///|])

preparePostForAnalysis :: CmdArgs -> FatPost -> FatPost
preparePostForAnalysis args = text %~ mappingFunc
  where
    mappingFunc = catMaybes [gabLinks] & foldl (>>>) id
    gabLinks = bool Nothing (Just removeGabPostLink) (args ^. removeGabLinks)

zipPostsWithExtractorResult :: ExtractorResult -> [(Post, ExtractorResult)]
zipPostsWithExtractorResult x = x & view posts <&> (, x)

postExtractorResultPairToFatPost :: (Post, ExtractorResult) -> FatPost
postExtractorResultPairToFatPost (Post {..}, ExtractorResult {..}) = FatPost {..}

scorePostPairs :: CmdArgs -> [(FatPost, FatPost)] -> AnalysisResult
scorePostPairs args xs = AnalysisResult {..}
  where
    _postsMatches = xs <&> scorePostPair <&> \y -> PostsMatch {_matches = [y]}
    scorePostPair :: (FatPost, FatPost) -> (FatPost, FatPost, Double)
    scorePostPair (p1, p2) = (p1, p2, calculateMatchingScore (prep p1) (prep p2))
    prep = preparePostForAnalysis args

connectMatchesFromMultipleNetworks :: AnalysisResult -> AnalysisResult
connectMatchesFromMultipleNetworks = id -- TODO: support for multiple networks

removeNonMatching :: Double -> AnalysisResult -> AnalysisResult
removeNonMatching threshold = postsMatches %~ processMatches
  where
    processMatches :: [PostsMatch] -> [PostsMatch]
    processMatches xs = xs <&> processPostsMatch & filter nonEmptyPM
    processPostsMatch :: PostsMatch -> PostsMatch
    processPostsMatch = matches %~ filter (\m -> m ^. _3 >= threshold)
    nonEmptyPM :: PostsMatch -> Bool
    nonEmptyPM x = x ^. matches & null & not

isWithinHoursDiff :: Int -> (FatPost, FatPost) -> Bool
isWithinHoursDiff h (p1, p2) = (diff & (/ 60) & floor & abs) < (toInteger h & (* 60))
  where
    dateFromPost = view date >>> parseDateTime
    ndt = diffUTCTime (dateFromPost p1) (dateFromPost p2)
    diff = ndt & toRational

removePairsWithTooBigHourDifference :: Int -> [(FatPost, FatPost)] -> [(FatPost, FatPost)]
removePairsWithTooBigHourDifference h = filter $ isWithinHoursDiff h

analyze :: CmdArgs -> [ExtractorResult] -> AnalysisResult
analyze args xs
 =
  xs <&> (posts %~ filterPostsForAnalysis) <&> zipPostsWithExtractorResult <&> fmap postExtractorResultPairToFatPost &
  combPairs &
  removePairsWithTooBigHourDifference (args ^. maxHoursDiff) &
  scorePostPairs args &
  removeNonMatching (args ^. scoreThreshold) &
  connectMatchesFromMultipleNetworks
