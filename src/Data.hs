{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}

module Data where

import           Utils
import           UtilsTH

import           Control.Lens  hiding ((&))
import           Data.Aeson.TH hiding (Options)
import           Data.Text     (Text)
import           GHC.Generics  (Generic)

data SocialNetworkType
  = Gab
  | Twitter
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''SocialNetworkType

deriveJSON derJsonOpts ''SocialNetworkType

data CmdArgs =
  CmdArgs
    { _firstFileName  :: Text
    , _secondFileName :: Text
    , _scoreThreshold :: Double
    , _removeGabLinks :: Bool
    , _maxHoursDiff   :: Int
    , _verbose        :: Bool
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CmdArgs

deriveJSON derJsonOpts ''CmdArgs

data Post =
  Post
    { _author              :: Text
    , _likes               :: Int
    , _reposts             :: Int
    , _replies             :: Int
    , _ratioLikesVsReplies :: Double
    , _text                :: Text
    , _isRepost            :: Bool
    , _dateRaw             :: Text
    , _date                :: Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''Post

deriveJSON derJsonOpts ''Post

data FatPost =
  FatPost
    { _author              :: Text
    , _likes               :: Int
    , _reposts             :: Int
    , _replies             :: Int
    , _ratioLikesVsReplies :: Double
    , _text                :: Text
    , _isRepost            :: Bool
    , _dateRaw             :: Text
    , _date                :: Text
    , _socialNetwork       :: SocialNetworkType
    , _obtainedAt          :: Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''FatPost

deriveJSON derJsonOpts ''FatPost

data ExtractorResult =
  ExtractorResult
    { _posts         :: [Post]
    , _userName      :: Text
    , _userTitle     :: Text
    , _userBio       :: Text
    , _socialNetwork :: SocialNetworkType
    , _obtainedAt    :: Text
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''ExtractorResult

deriveJSON derJsonOpts ''ExtractorResult

data PostsMatch =
  PostsMatch
    { _matches :: [(FatPost, FatPost, Double)]
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''PostsMatch

deriveJSON derJsonOpts ''PostsMatch

data AnalysisResult =
  AnalysisResult
    { _postsMatches :: [PostsMatch]
    }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''AnalysisResult

deriveJSON derJsonOpts ''AnalysisResult

data AppContext =
  AppContext
    { cmdArgs :: CmdArgs
    }
  deriving (Eq, Show, Generic)
