{-# LANGUAGE OverloadedStrings #-}

module Logging where

import           Data
import           Utils

import           Control.Lens
import           Control.Monad.Extra (skip)
import           Data.Bool           (bool)
import           Data.Text           (Text)
import qualified Data.Text           as T

makeLogger :: CmdArgs -> Text -> IO ()
makeLogger args x = bool skip (x & ("[SNA] " <>) & T.unpack & putStrLn) (args ^. verbose)
