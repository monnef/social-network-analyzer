{-# LANGUAGE OverloadedStrings #-}

module UtilsTH where

import           Control.Lens                  ((&))
import           Control.Lens.Internal.FieldTH (makeFieldOptics, _fieldToDef)
import           Control.Lens.TH
import           Data.Aeson.TH                 (defaultOptions,
                                                fieldLabelModifier)
import           Data.Char                     (toLower)
import           Language.Haskell.TH           (DecsQ, Name)

convertFieldName :: String -> String -> String
convertFieldName prefix x = x & drop (length prefix) & deCapitalizeWord

deCapitalizeWord :: String -> String
deCapitalizeWord "" = ""
deCapitalizeWord x  = (x & head & toLower & (: [])) <> (x & tail)

derJsonOpts = defaultOptions {fieldLabelModifier = drop 1}
